﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Checkers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Button> buttonList;

        public MainWindow()
        {
            InitializeComponent();

            InitBoard();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
        private void InitBoard()
        {
            buttonList = Board.Children.Cast<Button>().ToList();

            int counter = 0;

            buttonList.ForEach(button =>
            {
                if (counter < 12) 
                    button.Style = (Style)Resources["BlackPawn"];
                else if (counter > 19)
                    button.Style = (Style)Resources["WhitePawn"];

                counter++;
            }
            );
        }
    }
}
